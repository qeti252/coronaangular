import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodayDataComponent } from './components/todayData/todayData.component';


const routes: Routes = [
  { path: 'home', component: TodayDataComponent},
  { path: '', component: TodayDataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
