import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-todayData',
  templateUrl: './todayData.component.html',
  styleUrls: ['./todayData.component.css']
})
export class TodayDataComponent implements OnInit {

  tableData =[];
  loading: boolean = true;
  today: boolean = true;

  constructor( private dataService: DataService) { }

  ngOnInit() {
    this.getTodayData();
  }

  getTodayData() {
    this.loading = true;
    this.today = true;
    this.tableData = [];
    this.dataService.getTodayData()
      .subscribe( res => {
        res.forEach(data => 
          this.tableData.push(data)
          );
          this.loading = false;
      })
  }  
  getYesterdayData() {
    this.loading = true;
    this.today = false;
    this.tableData = [];
    this.dataService.getYesterdayData()
      .subscribe( res => {
        res.forEach(data => 
          this.tableData.push(data)
          );
          this.loading = false;
      })
  }  
}
