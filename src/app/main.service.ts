import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {
private _url = "http://10.10.10.13/Corona/Corona";


constructor(private http: HttpClient) {
 }

  url() {
    return this._url;
  }

}