import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MainService } from '../main.service';
import { HttpClient } from '@angular/common/http';
import { DataStatistic } from '../models/dataStatistic';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl;

constructor(
  private http: HttpClient,
  private mainService: MainService
) { 
  this.apiUrl = mainService.url();
}

getTodayData(): Observable<DataStatistic[]> {
    return this.http
    .get(`${this.apiUrl}/statistics_today`)
      .pipe((response: any) => response as Observable<DataStatistic[]>);
  }

  getYesterdayData(): Observable<DataStatistic[]> {
    return this.http
    .get(`${this.apiUrl}/statistics_yesterday`)
      .pipe((response: any) => response as Observable<DataStatistic[]>);
  }
}
