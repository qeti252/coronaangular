export interface DataStatistic {
    name: string,
    total_Cases: number,
    new_Cases: number,
    total_Deaths: number,
    new_Deaths: number,
    total_Recovered: number,
    active_Cases: number,
    serious_Critical: number,
    total_Cases_1M_Population: number,
    deaths_1M_Population: number,
    total_Tests: number,
    tests_1M_Population: number
}
